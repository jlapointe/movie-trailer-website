#!/usr/bin/env python
"""Instantiate Movie objects and pass to open_movies_page."""

import media
import fresh_tomatoes

# Instantiate the Movie objects containing movie metadata.
the_princess_bride = media.Movie(
    "The Princess Bride",
    "A tale of daring adventure through fantastic lands to save the kingdom \
    from a mad usurper.",
    "https://upload.wikimedia.org/wikipedia/en/d/db/Princess_bride.jpg",
    "https://www.youtube.com/watch?v=VYgcrny2hRs",
    "8.2/10")
gravity = media.Movie(
    "Gravity",
    "A struggle for survival in outer space.",
    "https://upload.wikimedia.org/wikipedia/en/f/f6/Gravity_Poster.jpg",
    "https://www.youtube.com/watch?v=ufsrgE0BYf0",
    "7.9/10")
shaun_of_the_dead = media.Movie(
    "Shaun of the Dead",
    "The zombie apocalypse forces a dysfunctional group of friends to team up \
    and survive -- or not.",
    "https://upload.wikimedia.org/wikipedia/en/e/ec/Shaun-of-the-dead.jpg",
    "https://www.youtube.com/watch?v=z-lmF5DAssU",
    "8.0/10")

# Store all the Movies in a list.
movies = [the_princess_bride, gravity, shaun_of_the_dead]

# Pass the movies list to be formatted and displayed.
fresh_tomatoes.open_movies_page(movies)

