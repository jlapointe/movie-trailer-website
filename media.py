#!/usr/bin/env python
class Movie():
    """Store and access movie metadata.

    This class stores the movie's metadata in the attributes listed below, and
    provides a method for opening the trailer video in a browser.

    Attributes:
        title (str): The title of the movie
        storyline (str): A brief description of the movie's storyline
        poster_image_url (str): A URL to an image of the movie poster
        trailer_youtube_url (str): A URL to the movie's trailer on Youtube
    """

    def __init__(self, title, storyline, poster_image_url, 
                 trailer_youtube_url, imdb_rating):
        """Assign the input to the appropriate class attributes"""
        self.title = title
        self.storyline = storyline
        self.poster_image_url = poster_image_url
        self.trailer_youtube_url = trailer_youtube_url
        self.imdb_rating = imdb_rating
        

    def show_trailer(self):
        """Open a web browser at the URL for the movie's trailer"""
        webbrowser.open(self.trailer_youtube_url)

